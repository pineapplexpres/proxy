/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taller4;

import java.util.ArrayList;
/**
 *
 * @author drosa
 */
public class Proxy implements IFacade {

        
    private Facade fachada = null;
    private ArrayList<Usuario> usuarios = new ArrayList<Usuario>();

    public Proxy(){
        usuarios.add(new Usuario("1","1","Administrador"));
    }
    
    public boolean ingresar(Usuario user) {
        int vc = 0;
        for (vc = 0; vc < usuarios.size(); vc++) {
            if (usuarios.get(vc).getLogin().equals(user.getLogin())
                    && usuarios.get(vc).getPassword().equals(user.getPassword())) {
                fachada = new Facade();
                fachada.ingresar(user);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean registrar(Usuario admin, Usuario user) {
        int vc = 0;
        for (vc = 0; vc < usuarios.size(); vc++) {
            if (usuarios.get(vc).getLogin().equals(admin.getLogin()) && 
                    usuarios.get(vc).getPassword().equals(admin.getPassword()) &&
                    usuarios.get(vc).getTipo().equals("Administrador")) {
                fachada = new Facade();
                fachada.registrar(admin,user);
                usuarios.add(user);
                return true;
            }
            
        }
        return false;
    }
}
