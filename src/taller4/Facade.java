/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taller4;

import java.util.ArrayList;

/**
 *
 * @author drosa
 */
public class Facade implements IFacade   {
    
    
    public Facade() {
    }
    
    
    @Override
    public boolean ingresar(Usuario user) {
        System.out.println("ok");
        return true;
    }

    @Override
    public boolean registrar(Usuario admin, Usuario nuevo) {
        System.out.println("OK");
        return true;
    }
}
