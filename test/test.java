
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import taller4.IFacade;
import taller4.Proxy;
import taller4.Usuario;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author drosa
 */
public class test {


    
    @Test 
    public void testRegistroT(){
        Usuario admin = new Usuario("1","1","Administrador");
        Usuario user = new Usuario("2","2","2");
        IFacade proxy = new Proxy (); 
        //System.out.println(proxy.registrar(admin, user));
        //assertTrue(true);
        assertTrue(proxy.registrar(admin, user)==true);
    }
    
    @Test 
    public void testRegistroF(){
        Usuario admin = new Usuario("2","1","Administrador");
        Usuario user = new Usuario("2","2","2");
        IFacade proxy = new Proxy (); 
        //System.out.println(proxy.registrar(admin, user));
        //assertTrue(true);
        assertTrue(proxy.registrar(admin, user)==false);
    }
    
    @Test 
    public void testIngresoT(){
        
        Usuario admin = new Usuario("1","1","Administrador");
        Usuario user = new Usuario("2","2","2");
        IFacade proxy = new Proxy ();
        proxy.registrar(admin, user);
        //System.out.println(proxy.ingresar(user));
        assertTrue(proxy.ingresar(user)==true);
    }
    
    @Test 
    public void testIngresoF(){
        
        Usuario admin = new Usuario("1","1","Administrador");
        Usuario user = new Usuario("2","2","2");
        IFacade proxy = new Proxy ();
        proxy.registrar(admin, user);
        Usuario userF = new Usuario("1","3","2");
        //System.out.println(proxy.ingresar(userF));
        assertTrue(proxy.ingresar(userF)==false);
    }
    
}
